#ifndef _ACTOR_HPP
#define _ACTOR_HPP

#include <SFML/Graphics.hpp>

#include <string>
#include <vector>
using namespace std;

class Actor
{
    public:
    Actor();
    void Draw( sf::RenderWindow& window );

    public:
    sf::Sprite m_body;
    sf::Sprite m_head;
    sf::Sprite m_arms;
    sf::Sprite m_face;
    sf::Sprite m_hair;
    sf::Sprite m_mouth_happy_talking;
    sf::Sprite m_mouth_happy_closed;
};

#endif

#ifndef _ACTOR_GENERATOR_HPP
#define _ACTOR_GENERATOR_HPP

#include <SFML/Graphics.hpp>

#include <vector>
#include <map>
using namespace std;

#include "../cpp-utilities/Logger.hpp"
#include "Actor.hpp"

struct BodyPart
{
    BodyPart() {}
    BodyPart( int t, int c, int v, string path )
    {
        type = t;
        color = c;
        variant = v;
        texture.loadFromFile( path );
    }

    int type;
    int color;
    int variant;
    sf::Texture texture;
};

struct BodyPartStats
{
    BodyPartStats()
    {
        totalTypes = 0;
        totalColors = 0;
        totalVariants = 0;
    }

    void Setup( int t, int c, int v )
    {
        totalTypes = t;
        totalColors = c;
        totalVariants = v;
    }

    int totalTypes;
    int totalColors;
    int totalVariants;
};

class ActorGenerator
{
    public:
    ActorGenerator();
    void Setup();

    Actor GenerateActor();

    private:
    int m_totalSkinTones;
    map<string, BodyPart> m_arms;
    map<string, BodyPart> m_bodies;
    map<string, BodyPart> m_faces;
    map<string, BodyPart> m_hair;
    map<string, BodyPart> m_heads;
    map<string, BodyPart> m_mouths;

    BodyPartStats m_armStats;
    BodyPartStats m_bodyStats;
    BodyPartStats m_faceStats;
    BodyPartStats m_hairStats;
    BodyPartStats m_headStats;
    BodyPartStats m_mouthStats;

    void LoadAssets();
};

#endif

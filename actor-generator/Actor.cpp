#include "Actor.hpp"

#include <SFML/Graphics.hpp>

Actor::Actor()
{
    m_arms.setPosition( sf::Vector2f( 0, 0 ) );
    m_body.setPosition( sf::Vector2f( 0, 0 ) );
    m_head.setPosition( sf::Vector2f( 0, 0 ) );
    m_face.setPosition( sf::Vector2f( 0, 0 ) );
    m_hair.setPosition( sf::Vector2f( 0, 0 ) );
    m_mouth_happy_talking.setPosition( sf::Vector2f( 0, 0 ) );
}

void Actor::Draw( sf::RenderWindow& window )
{
    window.draw( m_arms );
    window.draw( m_body );
    window.draw( m_head );
    window.draw( m_face );
    window.draw( m_hair );
    window.draw( m_mouth_happy_talking );
}

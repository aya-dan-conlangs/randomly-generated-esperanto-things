#include "ActorGenerator.hpp"

#include "../cpp-utilities/StringUtil.hpp"

#include <cstdlib>

ActorGenerator::ActorGenerator()
{
}

void ActorGenerator::Setup()
{
    Logger::Out( "Setup", "ActorGenerator::Setup" );
    LoadAssets();
}

Actor ActorGenerator::GenerateActor()
{
    Logger::Out( "Generate an actor!", "ActorGenerator::GenerateActor" );
    int skinTone = rand() % m_totalSkinTones + 1;
    int type;
    int color;
    int variant;
    string key;

    Actor actor;

    // Generate arms
    type = rand() % m_armStats.totalTypes + 1;
    variant = 0;

    key = "arms_type" + StringUtil::ToString( type )
        + "_color" + StringUtil::ToString( skinTone )
        + "_variant" + StringUtil::ToString( variant );
    actor.m_arms.setTexture( m_arms[key].texture );
    Logger::Out( "Arms: " + key, "ActorGenerator::GenerateActor" );

    // Generate body
    type        = (m_bodyStats.totalTypes == 0) ? 0 : rand() % m_bodyStats.totalTypes + 1;
    variant     = (m_bodyStats.totalVariants == 0) ? 0 : rand() % m_bodyStats.totalVariants + 1;

    key = "body_type" + StringUtil::ToString( type )
        + "_color" + StringUtil::ToString( skinTone )
        + "_variant" + StringUtil::ToString( variant );
    actor.m_body.setTexture( m_bodies[key].texture );
    Logger::Out( "Body: " + key, "ActorGenerator::GenerateActor" );

    // Generate head
    type        = (m_headStats.totalTypes == 0) ? 0 : rand() % m_headStats.totalTypes + 1;
    variant     = (m_headStats.totalVariants == 0) ? 0 : rand() % m_headStats.totalVariants + 1;

    key = "head_type" + StringUtil::ToString( type )
        + "_color" + StringUtil::ToString( skinTone )
        + "_variant" + StringUtil::ToString( variant );
    actor.m_head.setTexture( m_heads[key].texture );
    Logger::Out( "Head: " + key, "ActorGenerator::GenerateActor" );

    // Generate face
    type        = (m_faceStats.totalTypes == 0) ? 0 : rand() % m_faceStats.totalTypes + 1;
    color       = 0;
    variant     = (m_faceStats.totalVariants == 0) ? 0 : rand() % m_faceStats.totalVariants + 1;

    key = "face_type" + StringUtil::ToString( type )
        + "_color" + StringUtil::ToString( color )
        + "_variant" + StringUtil::ToString( variant );
    actor.m_face.setTexture( m_faces[key].texture );
    Logger::Out( "Face: " + key, "ActorGenerator::GenerateActor" );

    // Generate hair
    type        = (m_hairStats.totalTypes == 0) ? 0 : rand() % m_hairStats.totalTypes + 1;
    variant     = (m_hairStats.totalVariants == 0) ? 0 : rand() % m_hairStats.totalVariants + 1;

    key = "hair_type" + StringUtil::ToString( type )
        + "_color" + StringUtil::ToString( skinTone )
        + "_variant" + StringUtil::ToString( variant );
    actor.m_hair.setTexture( m_hair[key].texture );
    Logger::Out( "Hair: " + key, "ActorGenerator::GenerateActor" );

    // Generate mouth
    type        = (m_mouthStats.totalTypes == 0) ? 0 : rand() % m_mouthStats.totalTypes + 1;
    color       = 0;
    variant     = (m_mouthStats.totalVariants == 0) ? 0 : rand() % m_mouthStats.totalVariants + 1;

    key = "mouth_type" + StringUtil::ToString( type )
        + "_color" + StringUtil::ToString( color )
        + "_variant" + StringUtil::ToString( variant );
    actor.m_mouth_happy_talking.setTexture( m_mouths[key].texture );
    Logger::Out( "Mouth: " + key, "ActorGenerator::GenerateActor" );

    return actor;
}

void ActorGenerator::LoadAssets()
{
    string assetPath = "../assets/actors/parts/";
    Logger::Out( "Load Assets, base path is " + assetPath, "ActorGenerator::LoadAssets" );

    m_totalSkinTones = 3;

    // Load arms
    m_armStats.Setup( 1, 3, 0 );
    for ( int t = 1; t <= m_armStats.totalTypes; t++ )
    {
        for ( int c = 1; c <= m_armStats.totalColors; c++ )
        {
            string filename = "arms_type" + StringUtil::ToString( t )
                + "_color" + StringUtil::ToString( c )
                + ".png";

            string assetName = "arms_type" + StringUtil::ToString( t )
                + "_color" + StringUtil::ToString( c )
                + "_variant" + StringUtil::ToString( m_armStats.totalVariants );

            Logger::Out( "Setting up asset \"" + assetName + "\" with image \"" + assetPath + filename + "\"", "ActorGenerator::LoadAssets" );
            m_arms[assetName] = BodyPart( t, c, m_armStats.totalVariants, assetPath + filename );
        }
    }

    // Load bodies
    m_bodyStats.Setup( 2, 3, 2 );
    for ( int t = 1; t <= m_bodyStats.totalTypes; t++ )
    {
        for ( int c = 1; c <= m_bodyStats.totalColors; c++ )
        {
            for ( int v = 1; v <= m_bodyStats.totalVariants; v++ )
            {
                string filename = "body_type" + StringUtil::ToString( t )
                    + "_color" + StringUtil::ToString( c )
                    + "_variant" + StringUtil::ToString( v )
                    + ".png";

                string assetName = "body_type" + StringUtil::ToString( t )
                    + "_color" + StringUtil::ToString( c )
                    + "_variant" + StringUtil::ToString( v );

                Logger::Out( "Setting up asset \"" + assetName + "\" with image \"" + assetPath + filename + "\"", "ActorGenerator::LoadAssets" );
                m_bodies[assetName] = BodyPart( t, c, v, assetPath + filename );
            }
        }
    }

    // Load faces
    m_faceStats.Setup( 3, 0, 0 );
    for ( int t = 1; t <= m_faceStats.totalTypes; t++ )
    {
        string filename = "face_type" + StringUtil::ToString( t )
            + ".png";

        string assetName = "face_type" + StringUtil::ToString( t )
            + "_color" + StringUtil::ToString( m_faceStats.totalColors )
            + "_variant" + StringUtil::ToString( m_armStats.totalVariants );

        Logger::Out( "Setting up asset \"" + assetName + "\" with image \"" + assetPath + filename + "\"", "ActorGenerator::LoadAssets" );
        m_faces[assetName] = BodyPart( t, m_faceStats.totalColors, m_faceStats.totalVariants, assetPath + filename );
    }

    // Load hair
    m_hairStats.Setup( 3, 11, 0 );
    for ( int t = 1; t <= m_hairStats.totalTypes; t++ )
    {
        for ( int c = 1; c <= m_hairStats.totalColors; c++ )
        {
            string filename = "hair_type" + StringUtil::ToString( t )
                + "_color" + StringUtil::ToString( c )
                + ".png";

            string assetName = "hair_type" + StringUtil::ToString( t )
                + "_color" + StringUtil::ToString( c )
                + "_variant" + StringUtil::ToString( m_armStats.totalVariants );

            Logger::Out( "Setting up asset \"" + assetName + "\" with image \"" + assetPath + filename + "\"", "ActorGenerator::LoadAssets" );
            m_hair[assetName] = BodyPart( t, c, m_hairStats.totalVariants, assetPath + filename );
        }
    }

    // Load heads
    m_headStats.Setup( 1, 3, 0 );
    for ( int t = 1; t <= m_headStats.totalTypes; t++ )
    {
        for ( int c = 1; c <= m_headStats.totalColors; c++ )
        {
            string filename = "head_type" + StringUtil::ToString( t )
                + "_color" + StringUtil::ToString( c )
                + ".png";

            string assetName = "head_type" + StringUtil::ToString( t )
                + "_color" + StringUtil::ToString( c )
                + "_variant" + StringUtil::ToString( m_armStats.totalVariants );

            Logger::Out( "Setting up asset \"" + assetName + "\" with image \"" + assetPath + filename + "\"", "ActorGenerator::LoadAssets" );
            m_heads[assetName] = BodyPart( t, c, m_headStats.totalVariants, assetPath + filename );
        }
    }

    // Load mouths
    m_mouthStats.Setup( 1, 0, 2 );
    for ( int t = 1; t <= m_mouthStats.totalTypes; t++ )
    {
        for ( int v = 1; v <= m_mouthStats.totalVariants; v++ )
        {
            string filename = "mouth_type" + StringUtil::ToString( t )
                + "_variant" + StringUtil::ToString( v )
                + ".png";

            string assetName = "mouth_type" + StringUtil::ToString( t )
                + "_color" + StringUtil::ToString( m_mouthStats.totalColors )
                + "_variant" + StringUtil::ToString( v );

            Logger::Out( "Setting up asset \"" + assetName + "\" with image \"" + assetPath + filename + "\"", "ActorGenerator::LoadAssets" );
            m_mouths[assetName] = BodyPart( t, m_mouthStats.totalColors, v, assetPath + filename );
        }
    }
}

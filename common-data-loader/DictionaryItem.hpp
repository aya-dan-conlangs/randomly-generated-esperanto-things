#ifndef _DICTIONARY_ITEM_HPP
#define _DICTIONARY_ITEM_HPP

#include <string>
using namespace std;

enum WordType { NOUN, VERB, ADJECTIVE, ADVERB };

struct DictionaryItem
{
    DictionaryItem()
    {
        esperanto = "";
        english = "";
    }
    string esperanto;
    string english;
    WordType type;
};

#endif

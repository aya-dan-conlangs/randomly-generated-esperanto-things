#include "DataLoader.hpp"

#include "../cpp-utilities/Logger.hpp"

#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
using namespace std;

vector<DictionaryItem> DataLoader::nouns;
vector<DictionaryItem> DataLoader::verbs;
vector<DictionaryItem> DataLoader::adjectives;
vector<DictionaryItem> DataLoader::adverbs;

DictionaryItem DataLoader::GetRandomNoun()
{
    if ( nouns.size() == 0 ) {
        Logger::Error( "List is empty!", "DataLoader::GetRandomNoun" );
        return DictionaryItem();
    }
    return nouns[ rand() % nouns.size() ];
}

DictionaryItem DataLoader::GetRandomVerb()
{
    if ( verbs.size() == 0 ) {
        Logger::Error( "List is empty!", "DataLoader::GetRandomVerb" );
        return DictionaryItem();
    }
    return verbs[ rand() % verbs.size() ];
}

DictionaryItem DataLoader::GetRandomAdjective()
{
    if ( adjectives.size() == 0 ) {
        Logger::Error( "List is empty!", "DataLoader::GetRandomAdjective" );
        return DictionaryItem();
    }
    return adjectives[ rand() % adjectives.size() ];
}

DictionaryItem DataLoader::GetRandomAdverb()
{
    if ( adverbs.size() == 0 ) {
        Logger::Error( "List is empty!", "DataLoader::GetRandomAdverb" );
        return DictionaryItem();
    }
    return adverbs[ rand() % adverbs.size() ];
}

void DataLoader::Load( string path )
{
    srand( time( NULL ) );

    ifstream file;
    string buffer;

    {
    file.open( path + "esperanto-english_nouns.csv" );

    if ( file.fail() )
    {
        Logger::Error( "Could not open " + path + "!", "DataLoader::Load" );
    }

    bool first = true;
    while ( getline( file, buffer ) )
    {
        if ( first ) { first = false; continue; } // Skip the header
        vector<string> columns = DataLoader::Split( buffer );
        DictionaryItem item;
        item.english = columns[1];
        item.esperanto = columns[0];
        item.type = NOUN;
        DataLoader::nouns.push_back( item );
    }

    file.close();
    }


    {
    file.open( "../data-esperanto-dictionary/esperanto-english/esperanto-english_verbs.csv" );

    bool first = true;
    while ( getline( file, buffer ) )
    {
        if ( first ) { first = false; continue; } // Skip the header
        vector<string> columns = DataLoader::Split( buffer );
        DictionaryItem item;
        item.english = columns[1];
        item.esperanto = columns[0];
        item.type = NOUN;
        DataLoader::verbs.push_back( item );
    }

    file.close();
    }


    {
    file.open( "../data-esperanto-dictionary/esperanto-english/esperanto-english_adjectives.csv" );

    bool first = true;
    while ( getline( file, buffer ) )
    {
        if ( first ) { first = false; continue; } // Skip the header
        vector<string> columns = DataLoader::Split( buffer );
        DictionaryItem item;
        item.english = columns[1];
        item.esperanto = columns[0];
        item.type = NOUN;
        DataLoader::adjectives.push_back( item );
    }

    file.close();
    }


    {
    file.open( "../data-esperanto-dictionary/esperanto-english/esperanto-english_adverbs.csv" );

    bool first = true;
    while ( getline( file, buffer ) )
    {
        if ( first ) { first = false; continue; } // Skip the header
        vector<string> columns = DataLoader::Split( buffer );
        DictionaryItem item;
        item.english = columns[1];
        item.esperanto = columns[0];
        item.type = NOUN;
        DataLoader::adverbs.push_back( item );
    }

    file.close();
    }

    cout << DataLoader::nouns.size()        << " nouns loaded" << endl;
    cout << DataLoader::verbs.size()        << " verbs loaded" << endl;
    cout << DataLoader::adjectives.size()   << " adjectives loaded" << endl;
    cout << DataLoader::adverbs.size()      << " adverbs loaded" << endl;
}

vector<string> DataLoader::Split( string word )
{
    int delimiter = word.find( "," );

    string word1, word2;
    word1 = word.substr( 0+1, delimiter-2 );
    word2 = word.substr( delimiter+3, string::npos );
    word2 = word2.substr( 0, word2.size()-1 );

    vector<string> words;
    words.push_back( word1 );
    words.push_back( word2 );
    return words;
}

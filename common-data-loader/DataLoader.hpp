#ifndef _DATA_LOADER_HPP
#define _DATA_LOADER_HPP

#include <vector>
#include <string>
using namespace std;

#include "DictionaryItem.hpp"

class DataLoader
{
    public:
    static void Load( string path );
    static vector<DictionaryItem> nouns;
    static vector<DictionaryItem> verbs;
    static vector<DictionaryItem> adjectives;
    static vector<DictionaryItem> adverbs;

    static DictionaryItem GetRandomNoun();
    static DictionaryItem GetRandomVerb();
    static DictionaryItem GetRandomAdjective();
    static DictionaryItem GetRandomAdverb();

    private:
    static vector<string> Split( string word );
};

#endif

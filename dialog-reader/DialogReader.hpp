#ifndef _DIALOG_READER_HPP
#define _DIALOG_READER_HPP

#include <espeak/speak_lib.h>

#include <vector>
#include <string>
using namespace std;

class DialogReader
{
    public:
    DialogReader();
    ~DialogReader();

    // This might not be needed at all
    int SynthCallback( short *wav, int numsamples, espeak_EVENT *events );

    void Speak( string text, int voice );

    private:
    espeak_ERROR m_speakError;
    vector<espeak_VOICE> m_voices;

    void SetupVoices();
};

#endif

#include "DialogReader.hpp"

#include <iostream>
#include <string>
using namespace std;

#include <cstring>
#include <espeak/speak_lib.h>

#include "../cpp-utilities/Logger.hpp"

DialogReader::DialogReader()
{
    Logger::Out( "Init", "DialogReader Constructor" );

    if(espeak_Initialize(AUDIO_OUTPUT_SYNCH_PLAYBACK,0,NULL,espeakINITIALIZE_PHONEME_EVENTS) <0)
    {
        cout << "Error: Could not initialize espeak!" << endl;
        return;
    }

    //espeak_SetSynthCallback( DialogReader::SynthCallback );

    SetupVoices();
}

DialogReader::~DialogReader()
{
}

/*
typedef struct {
	const char *name;      // a given name for this voice. UTF8 string.
	const char *languages;       // list of pairs of (byte) priority + (string) language (and dialect qualifier)
	const char *identifier;      // the filename for this voice within espeak-data/voices
	unsigned char gender;  // 0=none 1=male, 2=female,
	unsigned char age;     // 0=not specified, or age in years
	unsigned char variant; // only used when passed as a parameter to espeak_SetVoiceByProperties
	unsigned char xx1;     // for internal use
	int score;       // for internal use
	void *spare;     // for internal use
} espeak_VOICE;
*/

void DialogReader::SetupVoices()
{
    // voice gender:
    espeak_VOICE voice1;
    voice1.gender = 1;
    voice1.languages = "eo";
    voice1.age = 15;
    m_voices.push_back( voice1 );

    espeak_VOICE voice2;
    voice2.gender = 2;
    voice2.languages = "eo";
    voice2.age = 30;
    m_voices.push_back( voice2 );

    espeak_VOICE voice3;
    voice3.gender = 1;
    voice3.languages = "eo";
    voice3.age = 50;
    m_voices.push_back( voice3 );

    cout << m_voices.size() << " voices loaded" << endl;
}

void DialogReader::Speak( string text, int voice )
{
    espeak_SetVoiceByProperties( &m_voices[ voice ] );

    /*
    ESPEAK_API espeak_ERROR espeak_Synth(const void *text,
	size_t size,
	unsigned int position,
	espeak_POSITION_TYPE position_type,
	unsigned int end_position,
	unsigned int flags,
	unsigned int* unique_identifier,
	void* user_data);
    */
    if((m_speakError=espeak_Synth(
        text.c_str(),                   // text
        strlen(text.c_str()),           // size
        0,                              // position
        POS_SENTENCE,                   // position_type
        0,                              // end_position
        espeakCHARS_AUTO,               // flags
        NULL,                           // unique_identifier
        NULL                            // user_data
        ))!= EE_OK)
    {
        cout << "Error on synth creation!" << endl;
    }
}


int DialogReader::SynthCallback(short *wav, int numsamples, espeak_EVENT *events)
{
    return 0;
}

#ifndef _STAGE_MANAGER_HPP
#define _STAGE_MANAGER_HPP

#include <SFML/Graphics.hpp>

#include <string>
using namespace std;

#include "../actor-generator/Actor.hpp"

class StageManager
{
    public:
    StageManager();
    ~StageManager();
    void Setup();

    void Clear();
    void Display();
    void AddActor( Actor actor );

    void Update();
    void Show( sf::Drawable& item );
    bool IsCurtainUp();
    void LowerCurtain();
    bool GetEvents( sf::Event& event );

    private:
    sf::Vector2i m_windowSize;
    string m_windowTitle;
    sf::RenderWindow m_window;
    vector<Actor> m_actors;
};

#endif


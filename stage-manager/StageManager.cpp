#include "StageManager.hpp"

#include "../cpp-utilities/Logger.hpp"

StageManager::StageManager()
{
}

StageManager::~StageManager()
{
}

void StageManager::Setup()
{
    Logger::Out( "Setup", "StageManager::Setup" );

    m_windowSize.x = 1280;
    m_windowSize.y = 720;
    m_windowTitle = "Randomly Generated Movie";

    m_window.create( sf::VideoMode( m_windowSize.x, m_windowSize.y ), m_windowTitle );
}

void StageManager::Clear()
{
    m_window.clear();
}

void StageManager::Display()
{
    m_window.display();
}

bool StageManager::IsCurtainUp()
{
    return m_window.isOpen();
}

void StageManager::LowerCurtain()
{
    m_window.close();
}

bool StageManager::GetEvents( sf::Event& event )
{
    return m_window.pollEvent( event );
}

void StageManager::Show( sf::Drawable& item )
{
    m_window.draw( item );
}

void StageManager::Update()
{
//    Logger::Out( "Update", "StageManager::Update" );
    for ( auto & actor : m_actors )
    {
        actor.Draw( m_window );
    }
}

void StageManager::AddActor( Actor actor )
{
    Logger::Out( "Add an actor!", "StageManager::AddActor" );
    m_actors.push_back( actor );
}

#include "../common-data-loader/DataLoader.hpp"
#include "../dialog-reader/DialogReader.hpp"

#include "DialogGenerator.hpp"
#include "../cpp-utilities/StringUtil.hpp"
#include "../cpp-utilities/Menu.hpp"

#include <iostream>
#include <fstream>
using namespace std;

int main()
{
//    DialogReader dr;
//    dr.Speak( "Saluton al vi!", 1 );
//    dr.Speak( "Saluton al vi!", 2 );
//    dr.Speak( "Saluton al vi!", 3 );

    char choice = 'n';
    bool done = false;
    string topic;
    string sentence;
    vector<Dialog> dialog;

    DataLoader::Load();

    cout << endl;

    DialogGenerator gen;

    int menuChoice = Menu::ShowIntMenuWithPrompt( { "Randomly choose topic", "Enter topic" } );
    if ( menuChoice == 1 )
    {
        while ( tolower( choice ) != 'y' )
        {
            DictionaryItem randomItem = DataLoader::GetRandomNoun();
            topic = randomItem.esperanto;

            cout << "Generate script about: " << randomItem.esperanto << " (" << randomItem.english << ")? (y/n): ";
            cin >> choice;
        }
    }
    else if ( menuChoice == 2 )
    {
        cout << "Enter in the topic noun (Esperanto): " << endl;
        getline( cin, topic );
    }

    int count = 1;
    while ( !done )
    {
        ofstream output( "generated-scripts/script_" + topic + "_" + StringUtil::ToString( count ) + ".txt" );
        count++;

        Menu::DrawHorizontalBar( 80 );
        cout << "La " << topic << endl << endl;
        output << "La " << topic << endl << endl;

        dialog.clear();
        dialog = gen.GenerateDialogAbout( topic, 4 );

        for ( auto& d : dialog )
        {
            cout << d.speaker << ": \"" << d.says << "\"" << endl << endl;
            output << d.speaker << ": \"" << d.says << "\"" << endl << endl;
        }

        char again;
        cout << "Again? (y/n): ";
        cin >> again;
        again = tolower( again );
        if ( again == 'n' )
        {
            done = true;
        }
        cout << endl;
    }

    return 0;
}

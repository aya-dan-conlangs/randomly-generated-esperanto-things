#include "DialogGenerator.hpp"

#include <iostream>
#include <fstream>
using namespace std;

#include "../cpp-utilities/Logger.hpp"
#include "../cpp-utilities/StringUtil.hpp"

DialogGenerator::DialogGenerator()
{
    m_dataPath = "";
}

void DialogGenerator::Setup()
{
    Logger::Out( "Setup", "DialogGenerator::Setup" );
    LoadTemplates();
}

void DialogGenerator::Setup( string dataPath )
{
    Logger::Out( "Setup", "DialogGenerator::Setup" );
    m_dataPath = dataPath;
    LoadTemplates();
}

vector<Dialog> DialogGenerator::GenerateDialogAbout( string topic, int items /* = 2 */ )
{
    Dialog d;
    string speaker1 = "Persono 1";
    string speaker2 = "Persono 2";

    vector<Dialog> dialog;

    for ( int i = 0; i < items; i++ )
    {
        string sentence = GetRandomStructure();
        sentence = FillNoun( sentence, topic );
        sentence = FillSpaces( sentence );

        if ( i % 2 == 0 )
        {
            d.speaker = speaker1;
            d.says = sentence;
        }
        else
        {
            d.speaker = speaker2;
            d.says = sentence;
        }

        dialog.push_back( d );
    }

    return dialog;
}

string DialogGenerator::GetRandomSentence()
{
    string sentence = GetRandomStructure();

    sentence = FillAdjectives( sentence );
    sentence = FillAdverbs( sentence );
    sentence = FillNouns( sentence );
    sentence = FillVerbs( sentence );

    return sentence;
}

string DialogGenerator::FillAdverbs( string sentence )
{
    int a, b;

    while ( sentence.find( "ADVERB" ) != string::npos )
    {
        a = sentence.find( "ADVERB" );
        b = string( "ADVERB" ).size();

        sentence = sentence.replace( a, b, DataLoader::GetRandomAdverb().esperanto );
    }

    return sentence;
}

string DialogGenerator::FillAdjectives( string sentence )
{
    int a, b;

    while ( sentence.find( "ADJECTIVE" ) != string::npos )
    {
        a = sentence.find( "ADJECTIVE" );
        b = string( "ADJECTIVE" ).size();

        sentence = sentence.replace( a, b, DataLoader::GetRandomAdjective().esperanto );
    }

    return sentence;
}

string DialogGenerator::FillNouns( string sentence )
{
    int a, b;

    while ( sentence.find( "NOUNN" ) != string::npos )
    {
        a = sentence.find( "NOUNN" );
        b = string( "NOUNN" ).size();

        sentence = sentence.replace( a, b, DataLoader::GetRandomNoun().esperanto + "n" );
    }

    while ( sentence.find( "NOUN" ) != string::npos )
    {
        a = sentence.find( "NOUN" );
        b = string( "NOUN" ).size();

        sentence = sentence.replace( a, b, DataLoader::GetRandomNoun().esperanto );
    }

    return sentence;
}

string DialogGenerator::FillVerbs( string sentence )
{
    int a, b;

    while ( sentence.find( "VERBAS" ) != string::npos )
    {
        a = sentence.find( "VERBAS" );
        b = string( "VERBAS" ).size();

        string infinitive = DataLoader::GetRandomVerb().esperanto;
        string presentTense = infinitive.replace( infinitive.size()-1, 1, "as" );

        sentence = sentence.replace( a, b, presentTense );
    }

    while ( sentence.find( "VERBIS" ) != string::npos )
    {
        a = sentence.find( "VERBIS" );
        b = string( "VERBIS" ).size();

        string infinitive = DataLoader::GetRandomVerb().esperanto;
        string presentTense = infinitive.replace( infinitive.size()-1, 1, "as" );

        sentence = sentence.replace( a, b, presentTense );
    }

    while ( sentence.find( "VERBOS" ) != string::npos )
    {
        a = sentence.find( "VERBOS" );
        b = string( "VERBOS" ).size();

        string infinitive = DataLoader::GetRandomVerb().esperanto;
        string presentTense = infinitive.replace( infinitive.size()-1, 1, "as" );

        sentence = sentence.replace( a, b, presentTense );
    }

    while ( sentence.find( "VERBI" ) != string::npos )
    {
        a = sentence.find( "VERBI" );
        b = string( "VERBI" ).size();

        sentence = sentence.replace( a, b, DataLoader::GetRandomVerb().esperanto );
    }

    while ( sentence.find( "VERBU" ) != string::npos )
    {
        a = sentence.find( "VERBU" );
        b = string( "VERBU" ).size();

        sentence = sentence.replace( a, b, DataLoader::GetRandomVerb().esperanto );
    }

    return sentence;
}

string DialogGenerator::FillNoun( string sentence, string noun )
{
    int a, b;

    if ( sentence.find( "NOUNN" ) != string::npos )
    {
        a = sentence.find( "NOUNN" );
        b = string( "NOUNN" ).size();

        sentence = sentence.replace( a, b, noun + "n" );
    }
    else if ( sentence.find( "NOUN" ) != string::npos )
    {
        a = sentence.find( "NOUN" );
        b = string( "NOUN" ).size();

        sentence = sentence.replace( a, b, noun );
    }

    return sentence;
}

string DialogGenerator::FillSpaces( string sentence )
{
    sentence = FillAdverbs( sentence );
    sentence = FillAdjectives( sentence );
    sentence = FillNouns( sentence );
    sentence = FillVerbs( sentence );
    return sentence;
}

void DialogGenerator::LoadTemplates()
{
    Logger::Out( "Setup", "DialogGenerator::LoadTemplates" );
    string path = m_dataPath + "sentence-forms.txt";
    ifstream input;
    input.open( path );

    if ( input.fail() )
    {
        Logger::Error( "Could not open " + path + "!", "DialogGenerator::LoadTemplates" );
    }

    string line;

    while ( getline( input, line ) )
    {
        m_sentenceForms.push_back( line );
    }

    Logger::Out( StringUtil::ToString( m_sentenceForms.size() ) + " sentence forms loaded", "DialogGenerator::LoadTemplates" );
}

string DialogGenerator::GetRandomStructure()
{
    return m_sentenceForms[ rand() % m_sentenceForms.size() ];
}

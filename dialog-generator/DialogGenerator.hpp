#ifndef _DIALOG_GENERATOR_HPP
#define _DIALOG_GENERATOR_HPP

#include "../common-data-loader/DataLoader.hpp"

#include <string>
using namespace std;

struct Dialog
{
    string speaker;
    string says;
};

class DialogGenerator
{
    public:
    DialogGenerator();
    void Setup();
    void Setup( string dataPath );

    string GetRandomSentence();
    vector<Dialog> GenerateDialogAbout( string topic, int items = 2 );

    private:
    string FillAdverbs( string sentence );
    string FillAdjectives( string sentence );
    string FillNouns( string sentence );
    string FillVerbs( string sentence );
    string FillNoun( string sentence, string noun );
    string FillSpaces( string sentence );

    string GetRandomStructure();
    void LoadTemplates();
    vector<string> m_sentenceForms;
    string m_dataPath;
};

#endif

#include <SFML/Graphics.hpp>

#include "../cpp-utilities/Logger.hpp"

#include "MovieGenerator.hpp"
#include "../dialog-reader/DialogReader.hpp"
#include "../dialog-generator/DialogGenerator.hpp"

#include <cstdlib>
#include <ctime>

int main()
{
    srand( time( NULL ) );
    Logger::Setup( true );

    MovieGenerator movieGenerator;
    movieGenerator.Run();

    system("pwd");
    DialogGenerator dg;
    DataLoader::Load( "../data-esperanto-dictionary/esperanto-english/" );
    dg.Setup( "../dialog-generator/data/" );

    DialogReader dr;
    vector<Dialog> dialogs = dg.GenerateDialogAbout( "kato", 4 );

    for ( auto & dialog : dialogs )
    {
        cout << dialog.speaker << ": " << dialog.says << endl;
        dr.Speak( dialog.says, 1 );
    }


    return 0;
}

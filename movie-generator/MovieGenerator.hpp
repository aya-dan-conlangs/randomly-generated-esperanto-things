#ifndef _MOVIE_GENERATOR_HPP
#define _MOVIE_GENERATOR_HPP

#include "../dialog-generator/DialogGenerator.hpp"
#include "../actor-generator/ActorGenerator.hpp"
#include "../stage-manager/StageManager.hpp"

class MovieGenerator
{
    public:
    MovieGenerator();
    void Setup();
    void Run();

    private:
    ActorGenerator m_actorGenerator;
    DialogGenerator m_dialogGenerator;

    StageManager m_stageManager;
};

#endif


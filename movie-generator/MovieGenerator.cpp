#include "MovieGenerator.hpp"

#include "../cpp-utilities/Logger.hpp"

MovieGenerator::MovieGenerator()
{
    Setup();
}

void MovieGenerator::Setup()
{
    Logger::Out( "Setup", "MovieGenerator::Setup" );
    m_stageManager.Setup();
    m_actorGenerator.Setup();
//    m_dialogGenerator.Setup();

    m_stageManager.AddActor( m_actorGenerator.GenerateActor() );
}

void MovieGenerator::Run()
{
    sf::RectangleShape shape;
    shape.setSize( sf::Vector2f( 1280, 720 ) );
    shape.setFillColor( sf::Color( 100, 100, 100 ) );

    while ( m_stageManager.IsCurtainUp() )
    {
        sf::Event event;
        while ( m_stageManager.GetEvents( event ) )
        {
            if ( event.type == sf::Event::Closed )
            {
                m_stageManager.LowerCurtain();
            }
        }

        m_stageManager.Clear();
        m_stageManager.Show( shape );
        m_stageManager.Update();
        m_stageManager.Display();
    }
}
